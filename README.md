## Getting Started

1. Clone the project

```bash
git clone git@gitlab.com:dovecancode/taskmanager-fe.git
```

2. Install the dependencies:

```bash
yarn
```

3. Run the development server:

```bash
yarn dev
```
